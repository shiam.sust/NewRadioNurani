package Utils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Util {
    //Database version
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "prayerDB";
    public static final String TABLE_NAME = "prayer";

    //contacts table columns names
    public static final String KEY_ID = "id";
    public static final String FAZAR = "fazar";
    public static final String ZOHOR = "zohor";
    public static final String ASAR = "asar";
    public static final String MAGRIB = "magrib";
    public static final String ESHA = "esha";
    public static final String SUNRISE = "sunrise";
    public static final String SUNSET = "sunset";
    public static final String ENG_DATE = "date_eng";
    public static final String HIJRI_DATE = "date_hijri";
    public static final String WEEKDAY = "day";
    public static final String CITY = "city";

    public static String currentDate(){
        SimpleDateFormat currentDate = new SimpleDateFormat("dd MMM yyyy");
        Date todayDate = new Date();
        String thisDate = currentDate.format(todayDate);
        return thisDate;
    }

}
