package Presenter;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.shiam.newradionurani.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.Date;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import Data.DatabaseHandler;
import Data.MySharedPreference;
import Model.PrayerListItem;
import Utils.Util;

public class NetworkingController {

    public String PRAYER_URL = "http://api.aladhan.com/v1/calendar?";
    private RequestQueue queue;
    Context mContext;
    DatabaseHandler db;
    TextView text;
    public View mView;

    public NetworkingController(Context context, View view) {
        mContext = context;
        queue = Volley.newRequestQueue(mContext);
        mView = view;
    }

    public String prepareParamsForPrayerApi(String latlon){
        String method = "2";
        String school = "1";

        //Getting date from system and spliting the date
        SimpleDateFormat currentDate = new SimpleDateFormat("dd/MM/yyyy");
        Date todayDate = new Date();
        String thisDate = currentDate.format(todayDate);
        String [] dateParts = thisDate.split("/");
        String day = dateParts[0];
        String month = dateParts[1];
        String year = dateParts[2];

        String param = latlon + "&" + "method=" + method + "&month=" + month + "&year=" + year + "&school=" + school;
        Log.d("testing","from prepare prayer data" + param);
        return param;
    }


    public void letsDoSomeNetworking(String params) {

        //http://api.aladhan.com
        // /v1/calendar?latitude=23.603603&longitude=90.351850&method=2&month=8&year=2018&school=1
        //PRAYER_URL = PRAYER_URL + params;
        //Log.d("url", PRAYER_URL+params);

        JsonObjectRequest jsonObjectRequest =
                new JsonObjectRequest(Request.Method.GET, PRAYER_URL+params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try{
                            db = new DatabaseHandler(mContext);
                            JSONArray schedulesArray = response.getJSONArray("data");
                            JSONObject dayOne = schedulesArray.getJSONObject(1);
                            JSONObject dateObject = dayOne.getJSONObject("date");

                            //to get month
                            JSONObject gregorianObject = dateObject.getJSONObject("gregorian");
                            JSONObject monthObj = gregorianObject.getJSONObject("month");
                            String month = monthObj.getString("en");
                            //Log.d("month", month);

                            //to get city
                            JSONObject metaObject = dayOne.getJSONObject("meta");
                            String city = metaObject.getString("timezone");
                            //Log.d("month", city);

                            //working with shared Preferences
                            MySharedPreference mySharedPreference = new MySharedPreference(mContext);
                            String prevMonth, prevCity;
                            prevMonth = mySharedPreference.getMonth();
                            prevCity = mySharedPreference.getCity();
                            Log.d("testing", "current city month : " + month + " " + month.length() +" " + city + " " + city.length());
                            Log.d("testing", "prev city month : " + prevMonth + " " + prevMonth.length() + " " + prevCity + " " + prevCity.length());
                            if(prevMonth.length() == 0 && prevCity.length() == 0){
                                mySharedPreference.saveData(month, city);
                                Log.d("testing", "data inserted into sPref");
                                addDataToSqliteDb(schedulesArray);
                                Toast.makeText(mContext, "data loaded successfully!", Toast.LENGTH_SHORT).show();

                            }else if(!month.matches(prevMonth) || !city.matches(prevCity)){
                                Log.d("testing", "month city not matched");
                                mySharedPreference.saveData(month, city);
                                db.deleteAll();
                                addDataToSqliteDb(schedulesArray);

                            }

                            //Log.d("mydata", response.toString());

                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("failed", error.getMessage());
            }
        });

    queue.add(jsonObjectRequest);

    }

    public void addDataToSqliteDb(JSONArray schedulesArray){
        try{
            for(int i = 0 ; i < schedulesArray.length() ; ++i){
                JSONObject day = schedulesArray.getJSONObject(i);
                JSONObject timings = day.getJSONObject("timings");
                JSONObject date = day.getJSONObject("date");
                JSONObject meta = day.getJSONObject("meta");

                JSONObject gregorian = date.getJSONObject("gregorian");
                JSONObject hijri = date.getJSONObject("hijri");
                JSONObject weekday = gregorian.getJSONObject("weekday");

                String fazar = timings.getString("Fajr");
                String zohor = timings.getString("Dhuhr");
                String asar = timings.getString("Asr");
                String magrib = timings.getString("Maghrib");
                String esha = timings.getString("Isha");
                String sunrise = timings.getString("Sunrise");
                String sunset = timings.getString("Sunset");
                String eng_date = date.getString("readable");
                String hijri_date = hijri.getString("date");
                String day_name = weekday.getString("en");
                String city_name = meta.getString("timezone");

                db.addToPrayer(new PrayerListItem(fazar, zohor, asar, magrib, esha, sunrise, sunset,
                        eng_date, hijri_date, day_name, city_name));
            }
        }catch (JSONException e){
            e.printStackTrace();
        }

    }

    /*public void updateUI(){
        text = mView.findViewById(R.id.prayer_time);
        mViewPager = (ViewPager) mView.findViewById(R.id.viewPager_id);
        updatedPrayerList = db.getAllPrayerTimes();

        text.setText("12:00:00");
        if(updatedPrayerList.size() > 0){
            PrayerListItem one = updatedPrayerList.get(0);
            zone = (TextView) mView.findViewById(R.id.zoneText);
            todayDate = (TextView) mView.findViewById(R.id.dateText);
            zone.setText("Zone: " + one.getCity_name());
            todayDate.setText(Util.currentDate());
        }

    }*/


}
