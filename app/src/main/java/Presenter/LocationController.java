package Presenter;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.util.List;

import Data.DatabaseHandler;
import Model.PrayerListItem;

public class LocationController{
    private LocationManager mLocationManager;
    private LocationListener mLocationListener;
    Context mContext;
    String longitude = null;
    String latitude = null;
    NetworkingController mNetworkingController;
    DatabaseHandler db;
    public View mView;

    public LocationController(Context context, View activity){
        mContext = context;
        mView = activity;
    }

    public void getCurrentLocation() {
        Log.d("testing", "getcurrentlocation method called");
        mLocationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
        mLocationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                Log.d("testing", location.toString());
                String lon = String.valueOf(location.getLongitude());
                String lat = String.valueOf(location.getLatitude());

                lon = lon.substring(0,9);
                lat = lat.substring(0,9);

                String latlon = "latitude=" + lat + "&longitude=" + lon ;
                String params;
                mNetworkingController = new NetworkingController(mContext, mView);
                params = mNetworkingController.prepareParamsForPrayerApi(latlon);
                mNetworkingController.letsDoSomeNetworking(params);
                Log.d("testing", "after prepare data item call");


            }
            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {
                Log.d("testing", "after prepare data item call");
            }

            @Override
            public void onProviderDisabled(String s) {

            }

        };

        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            ActivityCompat.requestPermissions((Activity) mContext, new String[] {Manifest.permission.ACCESS_COARSE_LOCATION},1);
            return;
        }
        mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5000, 1000  , mLocationListener);

    }


    public void setLatitude(String lat){
        this.latitude = lat;
    }

    public void setLongitude(String lon){
        this.longitude = lon;
    }

    public String getLongitude(){
        return longitude;
    }

    public String getLatitude(){
        return  latitude;
    }
}

