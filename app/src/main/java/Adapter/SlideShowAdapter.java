package Adapter;

import android.content.Context;
import android.media.Image;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.shiam.newradionurani.R;

import java.util.List;

import Data.DatabaseHandler;
import Model.PrayerListItem;
import Utils.Util;

public class SlideShowAdapter extends PagerAdapter
{
    private Context mContext;
    LayoutInflater inflater;
    List<PrayerListItem> prayerList;
    TextView prayerName, prayerTime;
    DatabaseHandler db;
    List<PrayerListItem> prayerListItem;
    String todayDate;

    public int[] images = {
            R.drawable.fazar,
            R.drawable.zohor,
            R.drawable.asar,
            R.drawable.magrib,
            R.drawable.esha
    };

    public SlideShowAdapter(Context context, List<PrayerListItem> prayerList) {
        mContext = context;
        this.prayerList = prayerList;
    }

    @Override
    public int getCount() {
        return images.length;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {

        return (view==(LinearLayout)object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        inflater = (LayoutInflater) mContext.getSystemService(mContext.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.slideshow_layout, container, false);
        ImageView img = (ImageView) view.findViewById(R.id.imageview_id);
        img.setImageResource(images[position]);
        prayerName = (TextView) view.findViewById(R.id.prayer_name);
        prayerTime = (TextView) view.findViewById(R.id.prayer_time);
        db = new DatabaseHandler(mContext);


        if(db != null){
            prayerListItem = db.getAllPrayerTimes();
            todayDate = Util.currentDate();

            Log.d("testing", "today is before: " + todayDate);

            for(PrayerListItem c : prayerListItem){
                //String log = "ID: " + c.getId() + " , Name: " + c.getFozor_time() + " , Phone: " + c.getZohor_time();
                //Log.d("testing" , log);
                String temp = c.getDate_eng();
                if(temp.matches(todayDate)){

                    //Log.d("testing", "today is: " + temp);
                    if(position == 0){
                        String temp2 = c.getFozor_time();
                        temp2 = temp2.substring(0, 5);
                        prayerTime.setText(temp2 + " AM");
                    }else if(position == 1){
                        String temp2 = c.getZohor_time();
                        temp2 = temp2.substring(0, 5);
                        String temp3 = temp2.substring(0, 2);
                        Log.d("timetemp", temp2 + " " +  temp3);
                        int temp4 = Integer.parseInt(temp3);
                        if(temp4 < 12){
                            prayerTime.setText(temp2 + " AM");
                        }else{
                            prayerTime.setText(temp2 + " PM");
                        }
                    }else if(position == 2){
                        String temp2 = c.getAsar_time();
                        temp2 = temp2.substring(0, 5);
                        prayerTime.setText(temp2 + " PM");
                    }else if(position == 3){
                        String temp2 = c.getMagrib_time();
                        temp2 = temp2.substring(0, 5);
                        prayerTime.setText(temp2 + " PM");
                    }else if(position == 4){
                        String temp2 = c.getEsha_time();
                        temp2 = temp2.substring(0, 5);
                        prayerTime.setText(temp2 + " PM");
                    }
                }

            }
        }

        if(position == 0){
            prayerName.setText("Fajar");
        }else if(position == 1){
            prayerName.setText("Johor");
        }else if(position == 2){
            prayerName.setText("Asar");
        }else if(position == 3){
            prayerName.setText("Magrib");
        }else if(position == 4){
            prayerName.setText("Esha");
        }
        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((LinearLayout)object);
    }
}
