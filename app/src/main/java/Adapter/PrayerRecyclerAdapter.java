package Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.shiam.newradionurani.R;

import java.util.List;

import Model.PrayerListItem;

public class PrayerRecyclerAdapter extends RecyclerView.Adapter<PrayerRecyclerAdapter.ViewHolder> {

    private Context context;
    private List<PrayerListItem> listItems;

    public PrayerRecyclerAdapter(Context context, List listitem){
        this.context = context;
        this.listItems = listitem;
    }

    @NonNull
    @Override
    public PrayerRecyclerAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.prayer_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PrayerRecyclerAdapter.ViewHolder holder, int position) {
        //here we will bind view to data souce to our recycler view
        PrayerListItem item = listItems.get(position);
        //holder.prayerIcon.setImageResource(item.getImage());
        //holder.prayerName.setText(item.getPrayer_name());
        //holder.prayerTime.setText(item.getPrayer_time());
    }

    @Override
    public int getItemCount() {
        return listItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder /*implements View.OnClickListener*/{
        public ImageView prayerIcon;
        public TextView prayerName;
        public TextView prayerTime;

        public ViewHolder(View itemView) {
            super(itemView);
            //itemView.setOnClickListener(this);
            prayerIcon = (ImageView) itemView.findViewById(R.id.weather_icon);
            prayerName = (TextView) itemView.findViewById(R.id.prayer_name);
            prayerTime = (TextView) itemView.findViewById(R.id.prayer_time);
        }

        /*@Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            PrayerListItem item = listItems.get(position);
            Intent intent = new Intent(context, Main2Activity.class);
            intent.putExtra("name", item.getName());
            intent.putExtra("description", item.getDescription());
            intent.putExtra("rating", item.getRating());
            context.startActivity(intent);

            Toast.makeText(context, item.getName(), Toast.LENGTH_SHORT).show();
        }*/
    }
}
