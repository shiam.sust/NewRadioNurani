package com.example.shiam.newradionurani;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.design.widget.TabLayout;
import android.os.Bundle;
import Adapter.ViewPagerAdapter;

public class MainActivity extends AppCompatActivity {
    TabLayout mTabLayout;
    ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mViewPager = (ViewPager) findViewById(R.id.viewPager_id);
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

        adapter.addFragment(new CurrentFragment(), "");
        adapter.addFragment(new PrayerFragment(), "");
        adapter.addFragment(new CompassFragment(), "");
        adapter.addFragment(new ScheduleFragment(), "");

        mViewPager.setAdapter(adapter);
        mTabLayout = (TabLayout) findViewById(R.id.tablayout_id);
        mTabLayout.setupWithViewPager(mViewPager);

        mTabLayout.getTabAt(0).setIcon(R.drawable.current2);
        mTabLayout.getTabAt(1).setIcon(R.drawable.prayer2);
        mTabLayout.getTabAt(2).setIcon(R.drawable.compass2);
        mTabLayout.getTabAt(3).setIcon(R.drawable.schedule2);


    }


}
