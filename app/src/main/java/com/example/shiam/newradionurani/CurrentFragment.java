package com.example.shiam.newradionurani;

import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import Presenter.LocationController;

public class CurrentFragment extends Fragment {

    Button play;
    private boolean playPause;
    private MediaPlayer mediaPlayer;
    private ProgressDialog progressDialog;
    private boolean initialStage = true;
    LocationController locationController;

    View view;
    public CurrentFragment(){

    }

    /*@Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.d("current", "on attach called");
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("current", "on create called");
    }*/

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.item_current, container, false);
        Log.d("current", "on create view called");
        locationController = new LocationController(getContext(), getView());
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        locationController.getCurrentLocation();
        //Log.d("current", "on activity created called");
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        progressDialog = new ProgressDialog(getContext());
        play = (Button) getView().findViewById(R.id.play);

        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!playPause) {

                    play.setBackgroundResource(R.drawable.pause);

                    if (initialStage) {
                        new Player().execute("http://220.247.165.153:8000/;?type=http&nocache=1");
                    } else {
                        if (!mediaPlayer.isPlaying())
                            mediaPlayer.start();
                    }

                    playPause = true;

                } else {
                    play.setBackgroundResource(R.drawable.play);

                    if (mediaPlayer.isPlaying()) {
                        mediaPlayer.pause();
                    }

                    playPause = false;
                }
            }
        });

    }

    class Player extends AsyncTask<String, Void, Boolean> {
        @Override
        protected Boolean doInBackground(String... strings) {
            Boolean prepared = false;

            try {
                mediaPlayer.setDataSource(strings[0]);
                mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mediaPlayer) {
                        initialStage = true;
                        playPause = false;
                        play.setBackgroundResource(R.drawable.play);
                        mediaPlayer.stop();
                        mediaPlayer.reset();
                    }
                });

                mediaPlayer.prepare();
                prepared = true;

            } catch (Exception e) {
                Log.e("MyAudioStreamingApp", e.getMessage());
                prepared = false;
            }

            return prepared;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);

            if (progressDialog.isShowing()) {
                progressDialog.cancel();
            }

            mediaPlayer.start();
            initialStage = false;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog.setMessage("Buffering...");
            progressDialog.show();
        }
    }

    /*
    @Override
    public void onStart() {
        super.onStart();
        Log.d("current", "on start called");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d("current", "on resume called");
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.d("current", "on save instant state called");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d("current", "on pause called");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d("current", "on stop called");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.d("current", "on destroy view called");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("current", "on distroy called");
    }*/
}
