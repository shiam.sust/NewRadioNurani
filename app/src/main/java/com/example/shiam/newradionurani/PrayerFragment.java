package com.example.shiam.newradionurani;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import Adapter.PrayerRecyclerAdapter;
import Adapter.SlideShowAdapter;
import Data.DatabaseHandler;
import Model.PrayerListItem;
import Presenter.LocationController;
import Presenter.PrayerController;
import Utils.Util;

public class PrayerFragment extends Fragment {
    View view;
    String strLat = null;
    String strLon = null;
    DatabaseHandler db;
    List<PrayerListItem> prayerList;
    TextView sunrise, sunset;
    String todayFromSys;

    //private RecyclerView recyclerView;
    //private RecyclerView.Adapter recyclerAdapter;
    private List<PrayerListItem> listItems;
    ViewPager viewPager;
    SlideShowAdapter adapter;
    TextView zone, todayDate;
    LocationController locationController;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        //Log.d("prayer", "on attach called");
    }

    /*
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("prayer", "on create called");
    }*/

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.item_prayer, container, false);
        //Log.d("prayer", "on create view called");
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d("testing", "on activity created called");
        locationController = new LocationController(getContext(), getView());
        locationController.getCurrentLocation();
        //String lat = locationController.getLatitude();
        //String lon = locationController.getLongitude();

        //Toast.makeText(getContext(), "Lat: "+lat + "Lon: "+lon, Toast.LENGTH_SHORT).show();
        db = new DatabaseHandler(getContext());

        if(db != null){
            prayerList = db.getAllPrayerTimes();
            todayFromSys = Util.currentDate();

            if(prayerList.size() > 0){
                PrayerListItem one = prayerList.get(0);
                zone = (TextView) getView().findViewById(R.id.zoneText);
                todayDate = (TextView) getView().findViewById(R.id.dateText);

                sunrise = (TextView) view.findViewById(R.id.sunrise_time);
                sunset = (TextView) view.findViewById(R.id.sunset_time);

                zone.setText("Zone: " + one.getCity_name());

            }
            for(PrayerListItem c : prayerList){
                String log = "ID: " + c.getId() + " date: " + c.getDate_eng() + " , fozor: " +
                        c.getFozor_time() + " , zohor: " + c.getZohor_time() + " sunrise: " + c.getSunrise_time()
                        + " sunset: " + c.getSunset_time();

                Log.d("testing" , log);

                String temp = c.getDate_eng();
                if(temp.matches(todayFromSys)){
                    //set today date
                    todayDate.setText(c.getDate_eng());

                    //setting sunset and sunrise time
                    String suntemp = c.getSunrise_time();
                    suntemp = suntemp.substring(0, 5);
                    sunrise.setText(suntemp + " AM");
                    suntemp = c.getSunset_time();
                    suntemp = suntemp.substring(0, 5);
                    sunset.setText(suntemp + " PM");
                }
            }
        }

        viewPager = (ViewPager) getView().findViewById(R.id.viewPager_id);
        adapter = new SlideShowAdapter(getContext(), prayerList);
        viewPager.setAdapter(adapter);

        listItems = new ArrayList<>();
        PrayerController prayer = new PrayerController();
        listItems = prayer.giveMeList();


    }

    public String currentDate(){
        SimpleDateFormat currentDate = new SimpleDateFormat("d MMM yyyy");
        Date todayDate = new Date();
        String thisDate = currentDate.format(todayDate);
        return thisDate;
    }

    @Override
    public void onStart() {
        super.onStart();
        //locationController.getCurrentLocation();
        Log.d("prayer", "on start called ");
    }


    @Override
    public void onResume() {
        super.onResume();
        Log.d("prayer", "on resume called prayer fragment");
    }

/*
    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.d("prayer", "on save instant state called");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d("prayer", "on create called");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d("prayer", "on create called");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.d("prayer", "on create called");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("prayer", "on create called");
    }*/
}
