package Model;

public class PrayerListItem {
    private int id;
    private String fozor_time;
    private String zohor_time;
    private String asar_time;
    private String magrib_time;
    private String esha_time;
    private String sunrise_time;
    private String sunset_time;
    private String date_eng;
    private String date_hijri;
    private String day;
    private String city_name;

    public PrayerListItem(){

    }

    public PrayerListItem(String fozor_time, String zohor_time, String asar_time, String magrib_time, String esha_time,
                          String sunrise_time, String sunset_time, String date_eng,
                          String date_hijri, String day, String city_name) {
        this.fozor_time = fozor_time;
        this.zohor_time = zohor_time;
        this.asar_time = asar_time;
        this.magrib_time = magrib_time;
        this.esha_time = esha_time;
        this.sunrise_time = sunrise_time;
        this.sunset_time = sunset_time;
        this.date_eng = date_eng;
        this.date_hijri = date_hijri;
        this.day = day;
        this.city_name = city_name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFozor_time() {
        return fozor_time;
    }

    public void setFozor_time(String fozor_time) {
        this.fozor_time = fozor_time;
    }

    public String getZohor_time() {
        return zohor_time;
    }

    public void setZohor_time(String zohor_time) {
        this.zohor_time = zohor_time;
    }

    public String getAsar_time() {
        return asar_time;
    }

    public void setAsar_time(String asar_time) {
        this.asar_time = asar_time;
    }

    public String getMagrib_time() {
        return magrib_time;
    }

    public void setMagrib_time(String magrib_time) {
        this.magrib_time = magrib_time;
    }

    public String getEsha_time() {
        return esha_time;
    }

    public void setEsha_time(String esha_time) {
        this.esha_time = esha_time;
    }

    public String getSunrise_time() {
        return sunrise_time;
    }

    public void setSunrise_time(String sunrise_time) {
        this.sunrise_time = sunrise_time;
    }

    public String getSunset_time() {
        return sunset_time;
    }

    public void setSunset_time(String sunset_time) {
        this.sunset_time = sunset_time;
    }

    public String getDate_eng() {
        return date_eng;
    }

    public void setDate_eng(String date_eng) {
        this.date_eng = date_eng;
    }

    public String getDate_hijri() {
        return date_hijri;
    }

    public void setDate_hijri(String date_hijri) {
        this.date_hijri = date_hijri;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getCity_name() {
        return city_name;
    }

    public void setCity_name(String city_name) {
        this.city_name = city_name;
    }
}
