package Data;

import android.content.Context;
import android.content.SharedPreferences;

public class MySharedPreference {

    Context mContext;
    private SharedPreferences myPrefs;
    private static final String PREFS_NAME = "myPrefsFile";
    String month = "";
    String city = "";

    public MySharedPreference(Context context){
        mContext = context;
    }

    public void saveData(String month, String city){
        myPrefs = mContext.getSharedPreferences(PREFS_NAME, 0);

        SharedPreferences.Editor editor = myPrefs.edit();

        editor.putString("month", month);
        editor.putString("city", city);

        editor.commit();
    }

    public String getMonth(){
        SharedPreferences prefs = mContext.getSharedPreferences(PREFS_NAME, 0);

        if(prefs.contains("month")){
            month = prefs.getString("month", "no") ;
        }
        return month;
    }

    public String getCity(){
        SharedPreferences prefs = mContext.getSharedPreferences(PREFS_NAME, 0);

        if(prefs.contains("city")){
            city = prefs.getString("city", "no") ;
        }
        return city;
    }
}
