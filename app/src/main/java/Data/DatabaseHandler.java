package Data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import Model.PrayerListItem;
import Utils.Util;

public class DatabaseHandler extends SQLiteOpenHelper {
    public DatabaseHandler(Context context) {
        super(context, Util.DATABASE_NAME, null, Util.DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_PRAYER_TABLE = "CREATE TABLE " + Util.TABLE_NAME + "(" + Util.KEY_ID + " INTEGER PRIMARY KEY, " +
                Util.FAZAR + " TEXT, " + Util.ZOHOR + " TEXT, " + Util.ASAR + " TEXT, " + Util.MAGRIB + " TEXT, " +
                Util.ESHA + " TEXT, " + Util.SUNRISE + " TEXT, " + Util.SUNSET + " TEXT, " + Util.ENG_DATE + " TEXT, " +
                Util.HIJRI_DATE + " TEXT, " + Util.WEEKDAY + " TEXT, " + Util.CITY + " TEXT)" ;

        db.execSQL(CREATE_PRAYER_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("DROP TABLE IF EXISTS " + Util.TABLE_NAME);
        onCreate(db);
    }

    //add prayer
    public void addToPrayer(PrayerListItem prayer){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues value = new ContentValues();
        value.put(Util.FAZAR, prayer.getFozor_time());
        value.put(Util.ZOHOR, prayer.getZohor_time());
        value.put(Util.ASAR, prayer.getAsar_time());
        value.put(Util.MAGRIB, prayer.getMagrib_time());
        value.put(Util.ESHA, prayer.getEsha_time());
        value.put(Util.SUNRISE, prayer.getSunrise_time());
        value.put(Util.SUNSET, prayer.getSunset_time());
        value.put(Util.ENG_DATE, prayer.getDate_eng());
        value.put(Util.HIJRI_DATE, prayer.getDate_hijri());
        value.put(Util.WEEKDAY, prayer.getDay());
        value.put(Util.CITY, prayer.getCity_name());

        //inserting into db
        db.insert(Util.TABLE_NAME, null, value);

        //closing the db
        db.close();

    }

    //delete all rows
    public void deleteAll(){
        SQLiteDatabase db = this.getWritableDatabase();
        String deleteAll = "delete from " + Util.TABLE_NAME;
        db.execSQL(deleteAll);
    }

    //get all contacts
    public List<PrayerListItem> getAllPrayerTimes(){
        SQLiteDatabase db = this.getReadableDatabase();
        List<PrayerListItem> prayerList = new ArrayList<>();

        String selectAll = "select * from " + Util.TABLE_NAME;
        Cursor cursor = db.rawQuery(selectAll, null);

        if(cursor.moveToFirst()){
            do{
                PrayerListItem prayer = new PrayerListItem();
                prayer.setId(Integer.parseInt(cursor.getString(0)));
                prayer.setFozor_time(cursor.getString(1));
                prayer.setZohor_time(cursor.getString(2));
                prayer.setAsar_time(cursor.getString(3));
                prayer.setMagrib_time(cursor.getString(4));
                prayer.setEsha_time(cursor.getString(5));
                prayer.setSunrise_time(cursor.getString(6));
                prayer.setSunset_time(cursor.getString(7));
                prayer.setDate_eng(cursor.getString(8));
                prayer.setDate_hijri(cursor.getString(9));
                prayer.setDay(cursor.getString(10));
                prayer.setCity_name(cursor.getString(11));

                prayerList.add(prayer);
            }while(cursor.moveToNext());
        }

        return prayerList;
    }
}
